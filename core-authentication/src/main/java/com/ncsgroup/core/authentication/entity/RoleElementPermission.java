package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.composite_key.RoleElementPermissionKey;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@Table(name = "role_element_permission")
@NoArgsConstructor
@AllArgsConstructor
public class RoleElementPermission {
  @EmbeddedId
  private RoleElementPermissionKey id;
  @ManyToOne(fetch = FetchType.LAZY) @MapsId("roleId")
  private Role role;
  @ManyToOne(fetch = FetchType.LAZY) @MapsId("elementId")
  private Element element;
  @ManyToOne(fetch = FetchType.LAZY) @MapsId("permissionId")
  private Permission permission;
  public static RoleElementPermission of(Role role, Element element, Permission permission) {
    return RoleElementPermission.builder()
          .id(new RoleElementPermissionKey(role.getId(), element.getId(), permission.getId()))
          .element(element)
          .role(role)
          .permission(permission)
          .build();
  }
}
