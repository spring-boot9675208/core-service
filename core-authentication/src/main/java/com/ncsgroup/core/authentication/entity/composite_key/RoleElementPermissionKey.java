package com.ncsgroup.core.authentication.entity.composite_key;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class RoleElementPermissionKey implements Serializable {
  private String roleId;
  private String elementId;
  private String permissionId;
}

