package com.ncsgroup.core.authentication.exception.permission;


import com.ncs.core.exception.exception.base.ConflictException;

public class DuplicatePermissionException extends ConflictException {

  public DuplicatePermissionException() {
    setCode("com.ncsgroup.core.authentication.exception.base.DuplicatePermissionException");
  }
}
