package com.ncsgroup.core.authentication.exception.user;


import com.ncs.core.exception.exception.base.ConflictException;

public class PhoneNumberAlreadyException extends ConflictException {

  public PhoneNumberAlreadyException() {
    setCode("com.ncsgroup.core.authentication.exception.user.PhoneNumberAlreadyException");
  }
}
