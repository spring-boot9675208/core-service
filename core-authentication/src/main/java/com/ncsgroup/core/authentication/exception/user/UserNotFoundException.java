package com.ncsgroup.core.authentication.exception.user;

import com.ncs.core.exception.exception.base.NotFoundException;

public class UserNotFoundException extends NotFoundException {

  public UserNotFoundException() {
    setCode("com.ncsgroup.core.authentication.exception.user.UserNotFoundException");
  }

  public UserNotFoundException(String id) {
    setCode("com.ncsgroup.core.authentication.exception.user.UserNotFoundException");
    addParam("name", id);
  }



}
