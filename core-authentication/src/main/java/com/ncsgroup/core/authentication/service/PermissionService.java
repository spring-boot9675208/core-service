package com.ncsgroup.core.authentication.service;

import com.ncsgroup.core.authentication.dto.request.permission.PermissionRequestDTO;
import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission;
import com.ncsgroup.core.authentication.entity.Permission;
import com.ncsgroup.core.authentication.service.base.BaseService;

import java.util.List;

public interface PermissionService extends BaseService<Permission> {
  List<PermissionResponseDTO> createBatch(List<PermissionRequestDTO> requestDTOs);

  List<PermissionResponseDTO> getAll();

  List<Permission> findByIdIn(List<String> ids);

  List<PermissionResponseDTO> listByElementId(String id);

  List<PermissionResponseDTO> getByElementAndRoleId(String elementId, String roleId);

  void delete(String id);

  PermissionResponseDTO update(String id, PermissionRequestDTO request);

  PermissionResponseDTO detail(String id);

  List<ShortcutPermission> getByElementIdAndRoleId(String elementId, String roleId);
}
