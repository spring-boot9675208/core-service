package com.ncsgroup.core.authentication.exception.element;

import com.ncs.core.exception.exception.base.NotFoundException;

public class ElementNotFoundException extends NotFoundException {

  public ElementNotFoundException() {
    setCode("com.ncsgroup.core.authentication.exception.element.ElementNotFoundException");
  }
}
