package com.ncsgroup.core.authentication.dto.request.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.VI_LANGUAGE;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UserRequestDTO {
  private String username;
  private String password;
  private String fullName;
  private String email;
  private String phoneNumber;
  private byte[] avatarLink;
  private String language = VI_LANGUAGE;
  private String theme;
  private int notificationEnable;
  private int lastNotificationId;
  private int isActive;
  private List<String> roleIds;
}
