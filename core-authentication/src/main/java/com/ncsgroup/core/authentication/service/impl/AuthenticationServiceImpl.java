package com.ncsgroup.core.authentication.service.impl;

import com.ncsgroup.core.authentication.dto.request.authen.RefreshTokenRequestDTO;
import com.ncsgroup.core.authentication.dto.response.authen.LoginResponseDTO;
import com.ncsgroup.core.authentication.dto.response.authen.TokenResponseDTO;
import com.ncsgroup.core.authentication.exception.token.TokenExpiredException;
import com.ncsgroup.core.authentication.exception.token.TokenInvalidException;
import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.service.AuthenticationService;
import com.ncsgroup.core.authentication.service.JwtTokenService;
import com.ncsgroup.core.authentication.service.TokenRedisService;
import com.ncsgroup.core.authentication.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;


@Service
@Slf4j
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
  private final UserService userService;
  private final JwtTokenService jwtTokenService;
  private final TokenRedisService tokenRedisService;
  private final AdministrationFacadeService administrationFacadeService;
  @Value("${token.expire-time-access-token}")
  private long expireTimeAccessToken;
  @Value("${token.expire-time-refresh-token}")
  private long expireTimeRefreshToken;

  @Override
  public LoginResponseDTO login(String username, String password) {
    log.info("(signIn)  username:   {}, password:    {}", username, password);

    var user = userService.getDetailUserByUsername(username);
    userService.equalPassword(password, user.getPassword());
    ShortcutUser shortcutUser = administrationFacadeService.getShortcutUser(user.getId());

    var claims = new HashMap<String, Object>();
    claims.put("username", username);
    claims.put("roles", shortcutUser.getRoles());
    String accessToken = jwtTokenService.generateAccessToken(
          user.getId(), claims);

    String refreshToken = jwtTokenService.generateRefreshToken(user.getId(), user.getUsername());
    tokenRedisService.set("refresh_token", user.getId(), refreshToken);

    return LoginResponseDTO.from(accessToken, refreshToken);
  }

  @Override
  public TokenResponseDTO refreshToken(RefreshTokenRequestDTO request) {
    log.info("(refreshToken)  RefreshTokenRequestDTO:   {}", request);
    String userId = jwtTokenService.getSubjectFromToken(request.getRefreshToken());


    String refreshTokenInRedis = (String) tokenRedisService.get("refresh_token", userId);
    String userIdInRedis = jwtTokenService.getSubjectFromToken(refreshTokenInRedis);

    if (jwtTokenService.getExpirationTime(refreshTokenInRedis) < jwtTokenService.getExpirationTime(request.getRefreshToken())) {
      log.error("(validateToken) ==========> TokenExpiredException");
      throw new TokenExpiredException();
    }
    if (userIdInRedis == null) {
      log.error("(validateToken) ==========> TokenInvalidException");
      throw new TokenInvalidException();
    }

    var user = userService.getDetailUserById(userId);
    ShortcutUser shortcutUser = administrationFacadeService.getShortcutUser(user.getId());

    var claims = new HashMap<String, Object>();
    claims.put("username", user.getUsername());
    claims.put("roles", shortcutUser.getRoles());
    String accessToken = jwtTokenService.generateAccessToken(user.getId(), claims);
    String refreshToken = jwtTokenService.generateRefreshToken(user.getId(), user.getUsername());
    log.info("jwt get from redis " + tokenRedisService.get("refresh_token", userId));
    tokenRedisService.set("refresh_token", userId, refreshToken);
    return TokenResponseDTO.from(accessToken, refreshToken, expireTimeAccessToken);
  }

}
