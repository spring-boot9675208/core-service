package com.ncsgroup.core.authentication.controller;

import com.ncsgroup.core.authentication.dto.request.element.ElementRequestDTO;
import com.ncsgroup.core.authentication.dto.response.ResponseGeneral;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.service.ElementService;
import com.ncsgroup.core.authentication.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.CREATE_ELEMENT_SUCCESS;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.SUCCESS;

@RestController
@RequestMapping("api/v1/elements")
@CrossOrigin
@RequiredArgsConstructor
@Slf4j
public class ElementController {
  private final ElementService elementService;
  private final MessageService messageService;
  private final AdministrationFacadeService facadeService;

  @PostMapping
  public ResponseGeneral<ElementResponseDTO> create(
        @RequestBody ElementRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {
    log.info("(create) request: {}", request);
    return ResponseGeneral.of(HttpStatus.CREATED.value(),
          messageService.getMessage(CREATE_ELEMENT_SUCCESS, language),
          facadeService.createElement(request));
  }

  @GetMapping
  public ResponseGeneral<List<ElementResponseDTO>> list(
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(list)");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          facadeService.listElements()
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete): {}", id);
    elementService.delete(id);
    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language));
  }

  @GetMapping("{id}")
  public ResponseGeneral<ElementResponseDTO> get(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(get): {}", id);
    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language), facadeService.getElement(id));
  }

  @PutMapping("{id}")
  public ResponseGeneral<ElementResponseDTO> update(
        @PathVariable String id,
        @RequestBody ElementRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("update) id: {}, request: {}", id, request);
    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language), facadeService.updateElement(id, request));
  }
}
