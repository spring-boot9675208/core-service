package com.ncsgroup.core.authentication.service;

import com.ncs.core.redis.BaseRedisHashService;

public interface TokenRedisService extends BaseRedisHashService<Object> {

}
