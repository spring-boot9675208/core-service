package com.ncsgroup.core.authentication.exception.element;

import com.ncs.core.exception.exception.base.BadRequestException;

public class ElementNameAlreadyExistException extends BadRequestException {

  public ElementNameAlreadyExistException() {
    setCode("com.ncsgroup.core.authentication.exception.element.ElementNameAlreadyExistException");
  }

  public ElementNameAlreadyExistException(String name) {
    setCode("com.ncsgroup.core.authentication.exception.element.ElementNameAlreadyExistException");
    addParam("name", name);
  }

}
