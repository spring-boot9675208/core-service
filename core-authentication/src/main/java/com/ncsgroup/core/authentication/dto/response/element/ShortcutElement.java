package com.ncsgroup.core.authentication.dto.response.element;

import com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ShortcutElement {
  private String id;
  private String code;
  private String name;
  private List<ShortcutPermission> permissions;

  public ShortcutElement(String id, String code, String name) {
    this.id = id;
    this.code = code;
    this.name = name;
  }

}
