package com.ncsgroup.core.authentication.service;

import com.ncsgroup.core.authentication.dto.request.user.ChangePasswordRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserUpdateRequestDTO;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.dto.response.user.UserDetailResponse;
import com.ncsgroup.core.authentication.dto.response.user.UserPageResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.entity.User;
import com.ncsgroup.core.authentication.service.base.BaseService;

import java.util.List;

public interface UserService extends BaseService<User> {
  UserResponseDTO create(UserRequestDTO request, List<RoleResponseDTO> roles);

  UserResponseDTO update(String id, UserUpdateRequestDTO request, List<RoleResponseDTO> roles);

  UserDetailResponse getDetailUserByUsername(String username);

  UserDetailResponse getDetailUserById(String id);


  UserPageResponseDTO list(int size, int page, String keyword, boolean isAll);

  void changePassword(ChangePasswordRequestDTO request);

  void remove(String id);

  UserResponseDTO detail(String id);

  void equalPassword(String passwordRaw, String passwordEncrypted);

  ShortcutUser getShortcutUser(String id);
}
