package com.ncsgroup.core.authentication.exception.user;


import com.ncs.core.exception.exception.base.BadRequestException;

public class WrongPasswordException extends BadRequestException {
  public WrongPasswordException() {
    setCode("com.ncsgroup.core.authentication.exception.user.WrongPasswordException");
  }

  public WrongPasswordException(String username) {
    setCode("com.ncsgroup.core.authentication.exception.user.WrongPasswordException");
    addParam("username", username);
  }
}
