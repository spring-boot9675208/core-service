package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "role")
@NoArgsConstructor
@AllArgsConstructor
public class Role extends BaseEntityWithUpdater {
  private String name;
  private String description;
  private int isActive;
  private int isSystem;
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
  private List<RoleElementPermission> roleElementPermissions = new ArrayList<>();
}
