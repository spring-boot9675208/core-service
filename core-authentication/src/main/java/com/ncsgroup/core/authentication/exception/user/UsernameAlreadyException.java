package com.ncsgroup.core.authentication.exception.user;


import com.ncs.core.exception.exception.base.ConflictException;

public class UsernameAlreadyException extends ConflictException {
  public UsernameAlreadyException(){
    setCode("com.ncsgroup.core.authentication.exception.user.UsernameAlreadyException");
  }
}
