package com.ncsgroup.core.authentication.service.impl;

import com.ncsgroup.core.authentication.dto.request.element.ElementRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.element.ShortcutElement;
import com.ncsgroup.core.authentication.entity.Element;
import com.ncsgroup.core.authentication.entity.Permission;
import com.ncsgroup.core.authentication.exception.element.ElementCodeAlreadyExistException;
import com.ncsgroup.core.authentication.exception.element.ElementNameAlreadyExistException;
import com.ncsgroup.core.authentication.exception.element.ElementNotFoundException;
import com.ncsgroup.core.authentication.repository.ElementRepository;
import com.ncsgroup.core.authentication.service.ElementService;
import com.ncsgroup.core.authentication.service.base.BaseServiceImpl;
import com.ncsgroup.core.authentication.utils.MapperUtils;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@Transactional
public class ElementServiceImpl extends BaseServiceImpl<Element> implements ElementService {

  private final ElementRepository repository;

  public ElementServiceImpl(ElementRepository elementRepository) {
    super(elementRepository);
    this.repository = elementRepository;
  }

  @Override
  public void checkExistByName(String name) {
    if (repository.existsByName(name)) {
      log.error("(checkExistByName) ======> ElementNameAlreadyExist name: {}", name);
      throw new ElementNameAlreadyExistException(name);
    }
  }

  @Override
  public void checkExitByCode(String code) {
    if (repository.existsByCode(code)) {
      log.error("(checkExistByCode) ======> ElementCodeAlreadyExist code: {}", code);
      throw new ElementCodeAlreadyExistException(code);
    }
  }

  @Override
  public void checkExistCodeOrNamePreUpdate(ElementRequestDTO request, Element element) {
    log.info("(checkExistCodeOrNamePreUpdate) request:{}, element:{}", request, element);
    boolean equalsName = request.getName().equals(element.getName());
    boolean equalsCode = request.getCode().equals(element.getCode());
    if (!equalsCode) checkExitByCode(request.getCode());
    if (!equalsName) checkExistByName(request.getName());
  }

  @Override
  public List<ElementResponseDTO> getAll() {
    log.info("(getAll)");
    return repository.list();
  }

  @Override
  public ElementResponseDTO update(String id, ElementRequestDTO request, List<Permission> permissions) {
    log.info("update) id: {}, request: {}, permissions: {}", id, request, permissions);
    Element existedElement = find(id);
    checkExistCodeOrNamePreUpdate(request, existedElement);
    Element element = MapperUtils.toEntity(request, Element.class);
    element.setId(id);
    element.setPermissions(permissions);
    return MapperUtils.toDTO(update(element), ElementResponseDTO.class);
  }

  @Override
  @Transactional
  public ElementResponseDTO create(ElementRequestDTO request, List<Permission> permissions) {
    log.info("(create) request:{}, permissions:{}", request, permissions);
    checkExistByName(request.getName());
    checkExitByCode(request.getCode());
    Element element = MapperUtils.toEntity(request, Element.class);
    element.setPermissions(permissions);
    return MapperUtils.toDTO(create(element), ElementResponseDTO.class);
  }

  @Override
  public ElementResponseDTO getdetail(String id) {
    log.info("(getDetail) id:{}", id);
    return MapperUtils.toDTO(find(id), ElementResponseDTO.class);
  }

  @Override
  public List<ElementResponseDTO> getByRoleId(String id) {
    log.info("(getByRoleId) id:{}", id);
    return MapperUtils.toDTOs(repository.listByRoleId(id), ElementResponseDTO.class);
  }

  @Override
  public List<ShortcutElement> listShortcutByRoleId(String roleId) {
    return repository.listShortcutByRoleId(roleId);
  }

  @Override
  public void delete(String id) {
    log.info("(delete)id: {}", id);
    repository.delete(find(id));
  }

  private Element find(String id) {
    log.info("(find) id:{}", id);
    return repository.findById(id).orElseThrow(ElementNotFoundException::new);
  }
}
