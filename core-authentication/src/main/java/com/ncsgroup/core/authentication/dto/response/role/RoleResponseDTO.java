package com.ncsgroup.core.authentication.dto.response.role;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class RoleResponseDTO {
  private String id;
  private String name;
  private String description;
  private int isActive;
  private int isSystem;
  private List<ElementResponseDTO> elements;

  public RoleResponseDTO(String id, String name, String description, int isActive, int isSystem) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.isSystem = isSystem;
  }
}
