package com.ncsgroup.core.authentication.dto.response.user;

import com.ncsgroup.core.authentication.dto.response.role.ShortcutRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ShortcutUser {
  private String id;
  private String username;
  private List<ShortcutRole> roles;

  public ShortcutUser(String id, String username) {
    this.id = id;
    this.username = username;
  }
}
