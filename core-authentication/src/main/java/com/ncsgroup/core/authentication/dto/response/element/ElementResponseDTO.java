package com.ncsgroup.core.authentication.dto.response.element;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import lombok.*;

import java.util.List;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ElementResponseDTO {
  private String id;
  private String name;
  private String code;
  private String description;
  private int isActive;
  private String elementIdentity;
  private List<PermissionResponseDTO> permissions;

  public ElementResponseDTO(String id, String name, String code, String description, int isActive, String elementIdentity) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.description = description;
    this.isActive = isActive;
    this.elementIdentity = elementIdentity;
  }
}
