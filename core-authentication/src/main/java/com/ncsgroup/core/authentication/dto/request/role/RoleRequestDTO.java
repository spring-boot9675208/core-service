package com.ncsgroup.core.authentication.dto.request.role;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.core.authentication.dto.request.element.ElementIdRequestDTO;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class RoleRequestDTO {
  private String name;
  private String description;
  private int isActive;
  private int isSystem;
  private List<ElementIdRequestDTO> elements;
}
