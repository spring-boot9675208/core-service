package com.ncsgroup.core.authentication.repository;

import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends BaseRepository<User> {
  boolean existsByUsername(String username);

  boolean existsByEmail(String email);

  boolean existsByPhoneNumber(String phoneNumber);

  @Query(value = "SELECT CASE WHEN EXISTS " +
        "(SELECT 1 FROM User u WHERE ( u.email = :email and :email is not null) or " +
        "(u.username = :username) or " +
        "(u.phoneNumber = :phone_number and :phone_number is not null)) THEN true ELSE false END ")
  boolean existsUser(
        @Param("email") String email,
        @Param("username") String username,
        @Param("phone_number") String phoneNumber
  );

  @Query(value = "UPDATE users SET password = :password WHERE id = :id", nativeQuery = true)
  void updatePassword(@Param("password") String password, @Param("id") String id);


  User findByUsername(String username);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO" +
        "(u.id, u.username, u.fullName, u.email, u.phoneNumber, u.isActive) " +
        "FROM User u WHERE (:keyword IS null " +
        "or lower(u.username) like %:keyword% " +
        "or lower(u.fullName) like %:keyword% " +
        "or u.phoneNumber like %:keyword% " +
        "or lower(u.email) like %:keyword% )")
  List<UserResponseDTO> search(String keyword, Pageable pageable);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO" +
        "(u.id, u.username, u.fullName, u.email, u.phoneNumber, u.isActive) " +
        "FROM User u ")
  List<UserResponseDTO> list();

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.user.ShortcutUser( " +
        " u.id, u.username ) FROM User u where u.id = :id")
  ShortcutUser getShortcutUser(String id);

  @Query(value = "SELECT count (u) " +
        "FROM User u WHERE (:keyword IS null " +
        "or lower(u.username) like %:keyword% " +
        "or lower(u.fullName) like %:keyword% " +
        "or u.phoneNumber like %:keyword% " +
        "or lower(u.email) like %:keyword% )")
  int countSearch(String keyword);


  @Query(value = "SELECT * FROM Users u where  u.id = ?1", nativeQuery = true)
  User findUser(String id);


  @Modifying
  @Transactional
  @Query(value = "INSERT INTO user_role(user_id, role_id) VALUES (:userId, :roleId)", nativeQuery = true)
  void insertUserRole(String userId, String roleId);

  @Modifying
  @Transactional
  @Query(value = "DELETE from user_role where user_id = :userId", nativeQuery = true)
  void deleteUserRoleByUserId(String userId);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO" +
        "(u.id, u.username, u.fullName, u.email, u.phoneNumber, u.isActive) " +
        "FROM User u where u.id = :id")
  UserResponseDTO getByUserId(String id);

}
