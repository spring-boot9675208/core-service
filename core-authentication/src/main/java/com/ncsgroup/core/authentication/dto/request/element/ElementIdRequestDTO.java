package com.ncsgroup.core.authentication.dto.request.element;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ElementIdRequestDTO {
  private String id;
  private List<String> permissionIds;
}
