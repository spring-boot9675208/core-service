package com.ncsgroup.core.authentication.controller;

import com.ncsgroup.core.authentication.dto.request.permission.PermissionRequestDTO;
import com.ncsgroup.core.authentication.dto.response.ResponseGeneral;
import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import com.ncsgroup.core.authentication.service.MessageService;
import com.ncsgroup.core.authentication.service.PermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.CREATE_ELEMENT_SUCCESS;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.SUCCESS;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/permissions")
public class PermissionController {
  private final PermissionService permissionService;
  private final MessageService messageService;

  @PostMapping
  public ResponseGeneral<Void> create(
        @RequestBody List<PermissionRequestDTO> requests,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(create) request: {}", requests);
    permissionService.createBatch(requests);
    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_ELEMENT_SUCCESS, language)
    );
  }

  @GetMapping
  public ResponseGeneral<List<PermissionResponseDTO>> list(
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(list)");

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          permissionService.getAll()
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete)");

    permissionService.delete(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }

  @PutMapping("{id}")
  public ResponseGeneral<PermissionResponseDTO> update(
        @PathVariable String id,
        @RequestBody PermissionRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("update) id: {}, request: {} ", id, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          permissionService.update(id, request)
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<PermissionResponseDTO> get(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("update) id: {} ", id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          permissionService.detail(id)
    );
  }
}
