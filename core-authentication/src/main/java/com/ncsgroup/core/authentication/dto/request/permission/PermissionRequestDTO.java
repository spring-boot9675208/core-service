package com.ncsgroup.core.authentication.dto.request.permission;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.ActiveStatus.ACTIVE;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PermissionRequestDTO {
  private String permissionName;
  private String code;
  private String description;
  private int isActive = ACTIVE;
  private String permissionIdentity;
}
