package com.ncsgroup.core.authentication.dto.response.permission;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ShortcutPermission {
  private String id;
  private String code;
  private String name;

  public ShortcutPermission(String id, String code, String name) {
    this.id = id;
    this.code = code;
    this.name = name;
  }
}
