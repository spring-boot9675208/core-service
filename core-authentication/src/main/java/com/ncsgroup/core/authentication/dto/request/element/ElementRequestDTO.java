package com.ncsgroup.core.authentication.dto.request.element;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ElementRequestDTO {
  private String name;
  private String code;
  private String description;
  private int isActive;
  private String elementIdentity;
  private List<String> permissionIds;
}
