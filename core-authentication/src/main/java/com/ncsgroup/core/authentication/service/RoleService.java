package com.ncsgroup.core.authentication.service;

import com.ncsgroup.core.authentication.dto.request.role.RoleRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.ShortcutRole;
import com.ncsgroup.core.authentication.entity.Role;
import com.ncsgroup.core.authentication.service.base.BaseService;

import java.util.List;


public interface RoleService extends BaseService<Role> {
  void checkExistRoleName(String name);

  List<RoleResponseDTO> getAll(String keyword, int page, int size, boolean isAll);
  void removeInRoleElementPermission(String id);
  RoleResponseDTO compositeRoleDTO(Role role);

  Role checkPreUpdate(String id, RoleRequestDTO request);

  RoleResponseDTO detail(String id);

  List<String> getRoleIdByUserId(String userId);

  ShortcutRole getShortcutRole(String id);

}
