package com.ncsgroup.core.authentication.repository;

import com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.ShortcutRole;
import com.ncsgroup.core.authentication.entity.Role;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends BaseRepository<Role> {
  boolean existsByName(String name);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO( " +
        " r.id, r.name, r.description, r.isActive, r.isSystem) from Role r"
  )
  List<RoleResponseDTO> listAll();

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.role.ShortcutRole( " +
        " r.id, r.name ) FROM Role r where r.id = :id")
  ShortcutRole getShortcutRole(String id);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO( " +
        " r.id, r.name, r.description, r.isActive, r.isSystem) " +
        "FROM Role r where lower(r.name) like %:keyword% " +
        "or lower(r.description) like %:keyword% "
  )
  List<RoleResponseDTO> search(Pageable pageable, String keyword);

  @Modifying
  @Query("DELETE FROM RoleElementPermission rep WHERE rep.role.id = :id")
  void deleteByRoleId(String id);

  @Query(value = "SELECT role_id FROM user_role WHERE user_id = ?1", nativeQuery = true)
  List<String> getRoleIdByUserId(String userId);
}
