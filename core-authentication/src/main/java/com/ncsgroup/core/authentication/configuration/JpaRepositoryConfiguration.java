package com.ncsgroup.core.authentication.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.ncsgroup.core.authentication"
)
public class JpaRepositoryConfiguration {
}
