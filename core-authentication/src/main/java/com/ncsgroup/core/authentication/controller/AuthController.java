package com.ncsgroup.core.authentication.controller;

import com.ncsgroup.core.authentication.dto.request.authen.LoginRequestDTO;
import com.ncsgroup.core.authentication.dto.request.authen.RefreshTokenRequestDTO;
import com.ncsgroup.core.authentication.dto.response.ResponseGeneral;
import com.ncsgroup.core.authentication.dto.response.authen.TokenResponseDTO;
import com.ncsgroup.core.authentication.service.AuthenticationService;
import com.ncsgroup.core.authentication.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.LOGIN_SUCCESS;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.SUCCESS;

@Slf4j
@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
  private final AuthenticationService authService;
  private final MessageService messageService;

  @PostMapping("login")
  public ResponseGeneral login(@RequestBody LoginRequestDTO request,
                               @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {
    log.info("(login)  request: {}", request);
    return ResponseGeneral.of(
          HttpStatus.OK.value(),
          messageService.getMessage(LOGIN_SUCCESS, language),
          authService.login(
                request.getUsername(),
                request.getPassword()
          )
    );
  }


  @PostMapping("/refresh")
  public ResponseGeneral<TokenResponseDTO> refreshToken(@RequestBody RefreshTokenRequestDTO request,
                                                        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {
    log.info("(refreshToken) request: {}", request);
    return ResponseGeneral.of(
          HttpStatus.OK.value(),
          messageService.getMessage(SUCCESS, language),
          authService.refreshToken(request)
    );
  }

}