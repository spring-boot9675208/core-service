package com.ncsgroup.core.authentication.exception.role;

import com.ncs.core.exception.exception.base.NotFoundException;

public class RoleNotFoundException extends NotFoundException {

  public RoleNotFoundException() {
    setCode("com.ncsgroup.core.authentication.exception.role.RoleNotFoundException");
  }
}
