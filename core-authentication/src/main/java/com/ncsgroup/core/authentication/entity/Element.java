package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.ActiveStatus.ACTIVE;


@Entity
@Getter
@Setter
@Table(name = "element")
@NoArgsConstructor
@AllArgsConstructor
public class Element extends BaseEntityWithUpdater {
  private String name;
  private String code;
  private String description;
  private int isActive = ACTIVE;
  private String elementIdentity;
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "element_permission",
        joinColumns = @JoinColumn(name = "element_id"),
        inverseJoinColumns = @JoinColumn(name = "permission_id"))
  private List<Permission> permissions = new ArrayList<>();
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "element")
  private List<RoleElementPermission> roleElementPermissions = new ArrayList<>();
}

