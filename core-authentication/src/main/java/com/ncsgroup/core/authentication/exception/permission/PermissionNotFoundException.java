package com.ncsgroup.core.authentication.exception.permission;

import com.ncs.core.exception.exception.base.NotFoundException;

public class PermissionNotFoundException extends NotFoundException {
  public PermissionNotFoundException() {
    setCode("com.ncsgroup.core.authentication.exception.ElementAlreadyExistException");
  }
}
