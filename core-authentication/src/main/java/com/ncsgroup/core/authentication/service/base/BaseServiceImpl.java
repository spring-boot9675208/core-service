package com.ncsgroup.core.authentication.service.base;
import com.ncs.core.exception.exception.base.NotFoundException;

import com.ncsgroup.core.authentication.repository.BaseRepository;

import java.util.List;

public class BaseServiceImpl<T> implements BaseService<T> {
  private final BaseRepository<T> repository;

  public BaseServiceImpl(BaseRepository<T> repository) {
    this.repository = repository;
  }

  @Override
  public T create(T t) {
    return repository.save(t);
  }

  @Override
  public T update(T t) {
    return repository.save(t);
  }

  @Override
  public void delete(String id) {repository.delete(get(id));}

  @Override
  public T get(String id) {
    return repository.findById(id).orElseThrow(NotFoundException::new);
  }

  @Override
  public List<T> list() {return repository.findAll();}
}
