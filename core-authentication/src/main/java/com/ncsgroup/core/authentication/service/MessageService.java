package com.ncsgroup.core.authentication.service;

public interface MessageService {
  String getMessage(String code, String language);
}
