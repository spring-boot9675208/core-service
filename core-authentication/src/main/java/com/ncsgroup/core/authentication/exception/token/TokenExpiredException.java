package com.ncsgroup.core.authentication.exception.token;

import com.ncs.core.exception.exception.base.BadRequestException;

public class TokenExpiredException extends BadRequestException {
    public TokenExpiredException() {
        setCode("com.ncsgroup.core.authentication.exception.token.TokenExpiredException");
    }
}
