package com.ncsgroup.core.authentication.controller;

import com.ncsgroup.core.authentication.dto.request.role.RoleRequestDTO;
import com.ncsgroup.core.authentication.dto.response.ResponseGeneral;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.service.MessageService;
import com.ncsgroup.core.authentication.service.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.CREATE_ROLE_SUCCESS;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.SUCCESS;

@Slf4j
@RestController
@RequestMapping("api/v1/roles")
@RequiredArgsConstructor
public class RoleController {
  private final MessageService messageService;
  private final AdministrationFacadeService facadeService;
  private final RoleService roleService;

  @PostMapping
  public ResponseGeneral<RoleResponseDTO> create(
        @RequestBody RoleRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(create) request: {}", request);

    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_ROLE_SUCCESS, language),
          facadeService.createRole(request)
    );
  }

  @GetMapping
  public ResponseGeneral<List<RoleResponseDTO>> list(
        @RequestParam(name = "keyword", required = false) String keyword,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestParam(name = "all", defaultValue = "false", required = false) boolean isAll,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(list)");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          facadeService.listRoles(keyword, page, size, isAll)
    );
  }

  @PutMapping("{id}")
  public ResponseGeneral<RoleResponseDTO> update(
        @PathVariable String id,
        @RequestBody RoleRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(update) id: {}, request: {}");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          facadeService.updateRole(id, request)
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<RoleResponseDTO> get(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(get) id: {}");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          facadeService.getDetailRole(id)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete) id: {}");
    roleService.delete(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }
}
