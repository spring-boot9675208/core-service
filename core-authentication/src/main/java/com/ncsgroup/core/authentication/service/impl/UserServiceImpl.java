package com.ncsgroup.core.authentication.service.impl;

import com.ncsgroup.core.authentication.dto.request.user.ChangePasswordRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserUpdateRequestDTO;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.dto.response.user.UserDetailResponse;
import com.ncsgroup.core.authentication.dto.response.user.UserPageResponseDTO;

import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.entity.User;
import com.ncsgroup.core.authentication.exception.authentication.PasswordIncorrectException;
import com.ncsgroup.core.authentication.exception.user.*;
import com.ncsgroup.core.authentication.repository.UserRepository;
import com.ncsgroup.core.authentication.service.UserService;
import com.ncsgroup.core.authentication.service.base.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

import static com.ncsgroup.core.authentication.utils.BCryptUtils.getPasswordEncoder;
import static com.ncsgroup.core.authentication.utils.MapperUtils.*;

@Slf4j
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

  private final UserRepository repository;
  private final PasswordEncoder passwordEncoder;

  public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
    super(repository);
    this.repository = repository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  @Transactional
  public UserResponseDTO create(UserRequestDTO request, List<RoleResponseDTO> roles) {
    log.info("(create) request: {}", request);

    checkExistUser(request.getUsername(), request.getEmail(), request.getPhoneNumber());
    User user = MODEL_MAPPER.map(request, User.class);
    user.setId(null);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    user.setPassword(getPasswordEncoder().encode(request.getPassword()));

    User savedUser = create(user);
    UserResponseDTO userResponseDTO = toDTO(savedUser, UserResponseDTO.class);
    userResponseDTO.setRoles(roles);

    request.getRoleIds().forEach(roleId -> repository.insertUserRole(user.getId(), roleId));

    return userResponseDTO;
  }

  @Override
  @Transactional
  public UserResponseDTO update(String id, UserUpdateRequestDTO request, List<RoleResponseDTO> roles) {
    log.info("(update) id: {}, request: {}", id, request);

    User user = find(id);
    checkExistPreUpdate(request, user);
    getModelMapper().map(request, user);
    user.setId(id);

    UserResponseDTO userResponseDTO = toDTO(update(user), UserResponseDTO.class);

    remove(id);
    request.getRoleIds().forEach(roleId -> repository.insertUserRole(id, roleId));

    userResponseDTO.setRoles(roles);
    return userResponseDTO;
  }


  @Override
  public UserDetailResponse getDetailUserByUsername(String username) {
    log.info("(getDetailUserByUsername) username:{}", username);
    User user = repository.findByUsername(username);
    if (user == null) {
      throw new UsernameAlreadyException();
    }
    return MODEL_MAPPER.map(user, UserDetailResponse.class);
  }

  @Override
  public UserDetailResponse getDetailUserById(String id) {
    User user = repository.findUser(id);
    if (user == null) {
      throw new UserNotFoundException();
    }
    return MODEL_MAPPER.map(user, UserDetailResponse.class);
  }

  @Override
  public UserPageResponseDTO list(int size, int page, String keyword, boolean isAll) {
    log.info("(list)keyword: {}, size : {}, page: {}, isAll: {}", keyword, size, page, isAll);
    List<UserResponseDTO> userResponseDTOs = isAll ?
          repository.list() :
          repository.search(keyword, PageRequest.of(page, size));

    return new UserPageResponseDTO(
          userResponseDTOs,
          isAll ? userResponseDTOs.size() : repository.countSearch(keyword)
    );

  }

  @Override
  @Transactional
  public void changePassword(ChangePasswordRequestDTO request) {
    log.info("(changePassword) request: {}", request);

    User userExisted = get(request.getUserId());
    equalPassword(request.getPassword(), userExisted.getPassword());
    userExisted.setPassword(getPasswordEncoder().encode(request.getNewPassword()));

  }

  @Override
  public void remove(String id) {
    log.info("(remove) id:{}", id);
    find(id);
    repository.deleteUserRoleByUserId(id);
  }

  @Override
  public UserResponseDTO detail(String id) {
    log.info("(detail):id:{}", id);
    return repository.getByUserId(id);
  }

  @Override
  public void equalPassword(String passwordRaw, String passwordEncrypted) {
    if (!getPasswordEncoder().matches(passwordRaw, passwordEncrypted)) {
      throw new PasswordIncorrectException();
    }
  }

  @Override
  public ShortcutUser getShortcutUser(String id) {
    return repository.getShortcutUser(id);
  }

  public void checkExistPreUpdate(UserUpdateRequestDTO request, User user) {
    log.info("(checkExistPreUpdate) ");

    if (Objects.nonNull(request.getUsername())
          && !request.getUsername().equals(user.getUsername())
          && repository.existsByUsername(request.getUsername())
    ) {
      throw new UsernameAlreadyException();
    }
    if (Objects.nonNull(request.getEmail())
          && !request.getEmail().equals(user.getEmail())
          && repository.existsByEmail(request.getEmail())
    ) {
      throw new EmailAlreadyException();
    }
    if (Objects.nonNull(request.getPhoneNumber())
          && !request.getPhoneNumber().equals(user.getPhoneNumber())
          && repository.existsByPhoneNumber(request.getPhoneNumber())
    ) {
      throw new PhoneNumberAlreadyException();
    }
  }

  public void checkExistUser(String username, String email, String phoneNumber) {
    log.info("(checkExistUser) username: {}, email: {}, phoneNumber: {}", username, email, phoneNumber);
    if (repository.existsUser(email, username, phoneNumber)) {
      throw new UserAlreadyException();
    }
  }

  private User find(String id) {
    log.info("(find) id:{}", id);
    return repository.findById(id).orElseThrow(UserNotFoundException::new);
  }

}
