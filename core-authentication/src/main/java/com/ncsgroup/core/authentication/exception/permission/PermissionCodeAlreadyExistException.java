package com.ncsgroup.core.authentication.exception.permission;


import com.ncs.core.exception.exception.base.BadRequestException;

public class PermissionCodeAlreadyExistException extends BadRequestException {

  public PermissionCodeAlreadyExistException() {
    setCode("com.ncsgroup.core.authentication.exception.permission.PermissionCodeAlreadyExistException");
  }

  public PermissionCodeAlreadyExistException(String code) {
    setCode("com.ncsgroup.core.authentication.exception.permission.PermissionCodeAlreadyExistException");
    addParam("code", code);
  }

}
