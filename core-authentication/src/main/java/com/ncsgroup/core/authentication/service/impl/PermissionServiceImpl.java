package com.ncsgroup.core.authentication.service.impl;

import com.ncsgroup.core.authentication.dto.request.permission.PermissionRequestDTO;
import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission;
import com.ncsgroup.core.authentication.entity.Permission;
import com.ncsgroup.core.authentication.exception.permission.DuplicatePermissionException;
import com.ncsgroup.core.authentication.exception.permission.PermissionCodeAlreadyExistException;
import com.ncsgroup.core.authentication.exception.permission.PermissionNameAlreadyExistException;
import com.ncsgroup.core.authentication.exception.permission.PermissionNotFoundException;
import com.ncsgroup.core.authentication.repository.PermissionRepository;
import com.ncsgroup.core.authentication.service.PermissionService;
import com.ncsgroup.core.authentication.service.base.BaseServiceImpl;
import com.ncsgroup.core.authentication.utils.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.ncsgroup.core.authentication.utils.MapperUtils.toDTOs;
import static com.ncsgroup.core.authentication.utils.MapperUtils.toEntities;

@Slf4j
public class PermissionServiceImpl extends BaseServiceImpl<Permission> implements PermissionService {
  private final PermissionRepository repository;

  public PermissionServiceImpl(PermissionRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Transactional
  public List<PermissionResponseDTO> createBatch(List<PermissionRequestDTO> requestDTOs) {
    log.info("(createBatch) request: {}", requestDTOs);
    List<Permission> permissions = toEntities(requestDTOs, Permission.class);
    return toDTOs(repository.saveAll(permissions), PermissionResponseDTO.class);
  }

  @Override
  public List<PermissionResponseDTO> getAll() {
    log.info("(getAll)");
    return repository.list();
  }

  @Override
  public List<Permission> findByIdIn(List<String> ids) {
    log.info("(findByIdIn) ids:{}", ids);
    checkListPermissionIds(ids);
    return repository.findByIdIn(ids);
  }

  private void checkListPermissionIds(List<String> permissionIds) {
    log.info("(checkListPermissionIds) permissionIds : {}", permissionIds);
    Set<String> listIds = new HashSet<>(permissionIds);
    if (!(permissionIds.size() == listIds.size())) throw new DuplicatePermissionException();
  }

  @Override
  public List<PermissionResponseDTO> listByElementId(String id) {
    log.info("(listByElementId) id: {}", id);
    return repository.listByElementId(id);
  }

  @Override
  public List<PermissionResponseDTO> getByElementAndRoleId(String elementId, String roleId) {
    log.info("(getByElementAndRoleId) elementId:{}, roleId:{}", elementId, roleId);
    return MapperUtils.toDTOs(repository.listByElementIdAndRoleId(elementId, roleId), PermissionResponseDTO.class);
  }

  @Override
  @Transactional
  public void delete(String id) {
    log.info("(delete) id: {}", id);
    find(id);
    repository.deleteInElementPermission(id);
    super.delete(id);
  }

  @Override
  public PermissionResponseDTO update(String id, PermissionRequestDTO request) {
    log.info("update) id: {}, request: {} ", id, request);
    Permission existedPermission = find(id);
    checkExistCodeOrNamePreUpdate(request, existedPermission);
    Permission permission = MapperUtils.toEntity(request, Permission.class);
    permission.setId(id);
    return MapperUtils.toDTO(update(permission), PermissionResponseDTO.class);
  }

  @Override
  public PermissionResponseDTO detail(String id) {
    log.info("(detail) id:{}", id);
    find(id);
    return MapperUtils.toDTO(get(id), PermissionResponseDTO.class);
  }

  @Override
  public List<ShortcutPermission> getByElementIdAndRoleId(String elementId, String roleId) {
    return repository.listByRoleIdAndElementId(elementId,roleId);
  }

  private void checkExistByName(String name) {
    if (repository.existsByName(name)) {
      log.error("(checkExistByName) ======> PermissionNameAlreadyExist name: {}", name);
      throw new PermissionNameAlreadyExistException(name);
    }
  }

  private void checkExistByCode(String code) {
    if (repository.existsByCode(code)) {
      log.error("(checkExistByCode) ======> PermissionCodeAlreadyExist name: {}", code);
      throw new PermissionCodeAlreadyExistException(code);
    }
  }

  private void checkExistCodeOrNamePreUpdate(PermissionRequestDTO request, Permission permission) {
    log.info("(checkExistCodeOrNamePreUpdate) request:{}, permission:{}", request, permission);
    boolean equalsName = request.getPermissionName().equals(permission.getName());
    boolean equalsCode = request.getCode().equals(permission.getCode());
    if (!equalsCode) checkExistByCode(request.getCode());
    if (!equalsName) checkExistByName(request.getPermissionName());
  }

  Permission find(String id) {
    log.info("(find) id:{}", id);
    return repository.findById(id).orElseThrow(PermissionNotFoundException::new);
  }

}
