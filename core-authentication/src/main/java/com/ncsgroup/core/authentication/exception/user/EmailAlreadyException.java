package com.ncsgroup.core.authentication.exception.user;


import com.ncs.core.exception.exception.base.ConflictException;

public class EmailAlreadyException extends ConflictException {

  public EmailAlreadyException() {
    setCode("com.ncsgroup.core.authentication.exception.user.EmailAlreadyException");
  }
}
