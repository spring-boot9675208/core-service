package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.composite_key.UserRoleKey;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@Table(name = "user_role")
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
  @EmbeddedId
  private UserRoleKey id;
  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("userId")
  private User user;
  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("roleId")
  private Role role;
}
