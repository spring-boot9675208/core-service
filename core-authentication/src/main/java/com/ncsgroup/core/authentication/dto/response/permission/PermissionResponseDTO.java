package com.ncsgroup.core.authentication.dto.response.permission;


import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PermissionResponseDTO {
  private String id;
  private String code;
  private String name;
  private String description;
  private int isActive;
  private String permissionIdentity;

  public PermissionResponseDTO(String id, String code, String name, String description, int isActive, String permissionIdentity) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.description = description;
    this.isActive = isActive;
    this.permissionIdentity = permissionIdentity;
  }
}
