package com.ncsgroup.core.authentication.service.impl;

import com.ncs.core.redis.BaseRedisHashServiceImpl;
import com.ncsgroup.core.authentication.service.TokenRedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

@Slf4j
public class TokenRedisServiceImpl extends BaseRedisHashServiceImpl<Object> implements TokenRedisService {

  public TokenRedisServiceImpl(RedisTemplate<String, Object> redisTemplate) {
    super(redisTemplate);
  }
}
