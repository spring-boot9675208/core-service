package com.ncsgroup.core.authentication.entity.base;


import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;


@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class BaseEntityWithUpdater extends BaseEntity {
  @LastModifiedBy
  private String lastUpdatedBy;
  @LastModifiedDate
  private Long lastUpdatedAt;

}


