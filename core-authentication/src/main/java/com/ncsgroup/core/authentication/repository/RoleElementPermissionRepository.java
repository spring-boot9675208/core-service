package com.ncsgroup.core.authentication.repository;

import com.ncsgroup.core.authentication.entity.RoleElementPermission;
import com.ncsgroup.core.authentication.entity.composite_key.RoleElementPermissionKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleElementPermissionRepository extends JpaRepository<RoleElementPermission, RoleElementPermissionKey> {

}
