package com.ncsgroup.core.authentication.exception.authentication;

import com.ncs.core.exception.exception.base.BadRequestException;



public class PasswordIncorrectException extends BadRequestException {
  public PasswordIncorrectException(){
    setCode("com.ncsgroup.core.authentication.exception.authentication.PasswordIncorrectException");
  }
}
