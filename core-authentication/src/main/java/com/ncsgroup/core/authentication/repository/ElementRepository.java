package com.ncsgroup.core.authentication.repository;

import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.element.ShortcutElement;
import com.ncsgroup.core.authentication.entity.Element;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ElementRepository extends BaseRepository<Element> {
  boolean existsByName(String name);

  boolean existsByCode(String code);

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO ( " +
        " e.id, e.code,e.name, e.description, e.isActive, e.elementIdentity ) " +
        "FROM Element  e ")
  List<ElementResponseDTO> list();

  @Query("SELECT DISTINCT rep.element FROM RoleElementPermission rep WHERE rep.role.id = :id")
  List<Element> listByRoleId(String id);


  @Query("SELECT DISTINCT new com.ncsgroup.core.authentication.dto.response.element.ShortcutElement(e.id, e.code, e.name) "
        + "FROM Element e join RoleElementPermission  rep where rep.role.id = :id and e.id = rep.element.id")
  List<ShortcutElement> listShortcutByRoleId(String id);
}