package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.ActiveStatus.ACTIVE;

@Entity
@Table(name = "permission")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Permission extends BaseEntityWithUpdater {
  private String name;
  private String code;
  private String description;
  private int isActive = ACTIVE;
  private String permissionIdentity;
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "permission")
  private List<RoleElementPermission> roleElementPermissions = new ArrayList<>();
}
