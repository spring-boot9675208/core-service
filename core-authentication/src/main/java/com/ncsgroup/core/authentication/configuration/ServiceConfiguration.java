package com.ncsgroup.core.authentication.configuration;


import com.ncs.core.exception.configuration.EnableCoreApiException;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.facade.impl.AdministrationFacadeServiceImpl;
import com.ncsgroup.core.authentication.repository.ElementRepository;
import com.ncsgroup.core.authentication.repository.PermissionRepository;
import com.ncsgroup.core.authentication.repository.RoleRepository;
import com.ncsgroup.core.authentication.repository.UserRepository;
import com.ncsgroup.core.authentication.service.*;
import com.ncsgroup.core.authentication.service.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ComponentScan(basePackages = {"com.ncsgroup.core.authentication"})
@EnableCoreApiException
public class ServiceConfiguration {

  @Bean
  public UserService userService(UserRepository repository, PasswordEncoder passwordEncoder) {
    return new UserServiceImpl(repository,passwordEncoder);
  }

  @Bean
  public PermissionService permissionService(PermissionRepository repository) {
    return new PermissionServiceImpl(repository);
  }

  @Bean
  public ElementService elementService(ElementRepository repository) {
    return new ElementServiceImpl(repository);
  }

  @Bean
  public RoleService roleService(RoleRepository repository) {
    return new RoleServiceImpl(repository);
  }

  @Bean
  public AdministrationFacadeService administratorFacadeService(
        ElementService elementService,
        PermissionService permissionService,
        RoleService roleService,
        UserService userService
  ) {
    return new AdministrationFacadeServiceImpl(elementService, permissionService, roleService, userService);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public TokenRedisService tokenRedisService(
        RedisTemplate<String, Object> redisTemplate
  ) {
    return new TokenRedisServiceImpl(redisTemplate);
  }
}
