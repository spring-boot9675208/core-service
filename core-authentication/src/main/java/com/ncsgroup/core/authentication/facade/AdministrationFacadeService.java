package com.ncsgroup.core.authentication.facade;

import com.ncsgroup.core.authentication.dto.request.element.ElementRequestDTO;
import com.ncsgroup.core.authentication.dto.request.role.RoleRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserUpdateRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.dto.response.user.UserPageResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.exception.element.ElementNotFoundException;

import java.util.List;

public interface AdministrationFacadeService {
  ElementResponseDTO createElement(ElementRequestDTO request);

  /**
   * Creates a new role in the system based on the information provided in the request.
   *
   * @param request The request containing the information needed to create the new role.
   *                The request includes the role name, description, isActive flag, and associated elements.
   *                The "elements" field is a list of "ElementId" objects that represent the elements
   *                associated with the new role.
   * @return A "RoleResponseDTO" object representing the newly created role. The response includes
   * the role id, name, description, isActive flag, and associated elements.
   * @throws SomeException If the request is invalid or if an error occurs while creating the role.
   */

  RoleResponseDTO createRole(RoleRequestDTO request);

  /**
   * Retrieves a list of all elements and their permissions in the system,
   * and maps them to a list of ElementFacadeResponseDTO objects.
   *
   * @return a list of ElementFacadeResponseDTO objects representing all elements in the system
   */
  List<ElementResponseDTO> listElements();

  /**
   * Retrieves a single element with the given ID, and maps it to an
   * ElementFacadeResponseDTO object.
   *
   * @param id the ID of the element to retrieve
   * @return an ElementFacadeResponseDTO object representing the element with the given ID
   * @throws ElementNotFoundException if no element with the given ID is found
   */
  ElementResponseDTO getElement(String id);

  /**
   * Updates an element with the given ID based on the information provided in the
   * ElementRequestDTO object, and maps the updated element to an ElementFacadeResponseDTO object.
   *
   * @param id      the ID of the element to update
   * @param request an ElementRequestDTO object containing the updated information for the element
   * @return an ElementFacadeResponseDTO object representing the updated element
   * @throws ElementNotFoundException if no element with the given ID is found
   */
  ElementResponseDTO updateElement(String id, ElementRequestDTO request);

  List<RoleResponseDTO> listRoles(String keyword, int page, int size, boolean isAll);

  RoleResponseDTO updateRole(String id, RoleRequestDTO request);

  RoleResponseDTO getDetailRole(String id);

  UserResponseDTO createUser(UserRequestDTO request);

  UserResponseDTO updateUser(String id, UserUpdateRequestDTO request);

  UserResponseDTO getDetailUser(String id);

  UserPageResponseDTO listUsers(int size, int page, String keyword, boolean isAll);

  ShortcutUser getShortcutUser(String id);

}
