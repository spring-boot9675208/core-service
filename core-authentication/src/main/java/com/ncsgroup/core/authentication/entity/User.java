package com.ncsgroup.core.authentication.entity;

import com.ncsgroup.core.authentication.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.ActiveStatus.ACTIVE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.VI_LANGUAGE;

@Data
@Entity
@Table(name = "users")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntityWithUpdater {
  private String username;
  private String password;
  private String fullName;
  private String email;
  private String phoneNumber;
  private byte[] avatarLink;
  private String language = VI_LANGUAGE;
  private String theme;
  private int notificationEnable;
  private int isActive = ACTIVE;
}

