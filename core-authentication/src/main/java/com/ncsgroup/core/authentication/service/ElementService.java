package com.ncsgroup.core.authentication.service;

import com.ncsgroup.core.authentication.dto.request.element.ElementRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.element.ShortcutElement;
import com.ncsgroup.core.authentication.entity.Element;
import com.ncsgroup.core.authentication.entity.Permission;
import com.ncsgroup.core.authentication.service.base.BaseService;

import java.util.List;


public interface ElementService extends BaseService<Element> {
  void checkExistByName(String name);

  void checkExitByCode(String code);

  void checkExistCodeOrNamePreUpdate(ElementRequestDTO request, Element element);

  List<ElementResponseDTO> getAll();

  ElementResponseDTO update(String id, ElementRequestDTO request, List<Permission> permissions);

  ElementResponseDTO create(ElementRequestDTO request, List<Permission> permissions);

  ElementResponseDTO getdetail(String id);

  List<ElementResponseDTO> getByRoleId(String id);

  List<ShortcutElement> listShortcutByRoleId(String roleId);
}
