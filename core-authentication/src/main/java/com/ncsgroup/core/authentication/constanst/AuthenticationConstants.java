package com.ncsgroup.core.authentication.constanst;

public class AuthenticationConstants {

  public static class CommonConstants {
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String LANGUAGE = "Accept-Language";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String VI_LANGUAGE = "vi";
  }


  public static class AuditorConstant {
    public static final String ANONYMOUS = "anonymousUser";
    public static final String SYSTEM = "SYSTEM";
  }

  public static class StatusException {
    public static final Integer NOT_FOUND = 404;
    public static final Integer CONFLICT = 409;
    public static final Integer BAD_REQUEST = 400;
  }

  public static class AuthConstant {
    public static String TYPE_TOKEN = "Bearer ";
    public static String AUTHORIZATION = "Authorization";
    public static final int AUTHORIZATION_TYPE_SIZE = 7;
    public static final String INVALID_TOKEN = "Token is invalid";
    public static final String EXPIRED_TOKEN = "Token is expired";
  }

  public static final class ActiveStatus {
    public static final int INACTIVE = 0;
    public static final int ACTIVE = 1;
  }

  public static class ValidationMessage {
    public static final String CONFIRM_PASSWORD_NOT_MATCH = "com.ncsgroup.core.authentication.validator.ValidationConfirmPassword";
  }

  public static class MessageCode {
    public static final String SUCCESS = "success";
    public static final String CREATE_ELEMENT_SUCCESS = "com.ncsgroup.core.authentication.controller.ElementController.createElement";
    public static final String LIST_ELEMENT_SUCCESS = "com.ncsgroup.core.authentication.controller.ElementController.listElement";
    public static final String CREATE_PERMISSION_SUCCESS = "com.ncsgroup.core.authentication.controller.PermissionController.create";
    public static final String CREATE_ROLE_SUCCESS = "com.ncsgroup.authentication.controller.admin.RoleController.create";
    public static final String CREATE_USER_SUCCESS = "com.ncsgroup.core.authentication.controller.UserController.create";
    public static final String LOGIN_SUCCESS = "com.ncsgroup.core.authentication.controller.AuthController.login";
    public static final String CHANGE_PASSWORD_SUCCESS = "com.ncsgroup.core.authentication.controller.UserController.changePassword";
  }
}
