package com.ncsgroup.core.authentication.facade.impl;

import com.ncsgroup.core.authentication.dto.request.element.ElementIdRequestDTO;
import com.ncsgroup.core.authentication.dto.request.element.ElementRequestDTO;
import com.ncsgroup.core.authentication.dto.request.role.RoleRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserUpdateRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.element.ShortcutElement;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.ShortcutRole;
import com.ncsgroup.core.authentication.dto.response.user.ShortcutUser;
import com.ncsgroup.core.authentication.dto.response.user.UserPageResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.entity.*;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.service.ElementService;
import com.ncsgroup.core.authentication.service.PermissionService;
import com.ncsgroup.core.authentication.service.RoleService;
import com.ncsgroup.core.authentication.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.ncsgroup.core.authentication.utils.MapperUtils.toEntity;

@Slf4j
@RequiredArgsConstructor
public class AdministrationFacadeServiceImpl implements AdministrationFacadeService {
  private final ElementService elementService;
  private final PermissionService permissionService;
  private final RoleService roleService;
  private final UserService userService;

  @Override
  public RoleResponseDTO createRole(RoleRequestDTO request) {
    log.info("(createRole) request: {}", request);
    roleService.checkExistRoleName(request.getName());
    Role role = toEntity(request, Role.class);
    if (Objects.nonNull(request.getElements())) {
      role.setRoleElementPermissions(buildRoleObjectPermissions(role, request.getElements()));
    }

    return roleService.compositeRoleDTO(roleService.create(role));
  }

  @Override
  public List<ElementResponseDTO> listElements() {
    log.info("(listAllElements)");
    List<ElementResponseDTO> elementResponseDTOS = elementService.getAll();
    elementResponseDTOS.forEach(elementResponseDTO ->
          elementResponseDTO.setPermissions(permissionService.listByElementId(elementResponseDTO.getId())));
    return elementResponseDTOS;
  }

  @Override
  public List<RoleResponseDTO> listRoles(String keyword, int page, int size, boolean isAll) {
    log.info("(list)keyword: {}, page : {}, size: {}, isAll: {}", keyword, page, size, isAll);
    List<RoleResponseDTO> roleResponseDTOS = roleService.getAll(keyword, page, size, isAll);
    roleResponseDTOS.forEach(roleResponseDTO -> setElementAndPermissionForRole(roleResponseDTO));
    return roleResponseDTOS;
  }

  @Override
  public ElementResponseDTO getElement(String id) {
    log.info("(getElement) id: {}", id);
    return elementService.getdetail(id);
  }

  @Override
  public ElementResponseDTO updateElement(String id, ElementRequestDTO request) {
    log.info("updateElement) id: {}, request: {}", id, request);
    return elementService.update(id, request, permissionService.findByIdIn(request.getPermissionIds()));
  }

  @Override
  public RoleResponseDTO updateRole(String id, RoleRequestDTO request) {
    log.info("(updateRole) id: {}, request: {}", id, request);
    Role role = roleService.checkPreUpdate(id, request);
    role.setId(id);
    if (Objects.nonNull(request.getElements())) {
      role.setRoleElementPermissions(buildRoleObjectPermissions(role, request.getElements()));
    }
    return roleService.compositeRoleDTO(roleService.update(role));
  }

  @Override
  public RoleResponseDTO getDetailRole(String id) {
    log.info("(getRole) id :{}", id);
    RoleResponseDTO roleResponseDTO = roleService.detail(id);
    setElementAndPermissionForRole(roleResponseDTO);
    return roleResponseDTO;
  }

  public List<RoleResponseDTO> getRolesByIdIn(List<String> roleIds) {
    log.info("(getRolesByIdIn) roleIds:{}", roleIds);
    List<RoleResponseDTO> roleResponseDTOS = new ArrayList<>();
    roleIds.forEach(id -> roleResponseDTOS.add(getDetailRole(id)));
    return roleResponseDTOS;
  }

  @Override
  public UserResponseDTO createUser(UserRequestDTO request) {
    log.info("(createUser) request : {}", request);
    List<RoleResponseDTO> roleResponseDTOS = getRolesByIdIn(request.getRoleIds());
    return userService.create(request, roleResponseDTOS);
  }

  @Override
  public UserResponseDTO updateUser(String id, UserUpdateRequestDTO request) {
    log.info("(updateUser) id : {}, request : {}", id, request);
    return userService.update(id, request, getRolesByIdIn(request.getRoleIds()));
  }

  @Override
  public UserResponseDTO getDetailUser(String id) {
    log.info("(getDetailUser) id:{}", id);
    UserResponseDTO userResponseDTO = userService.detail(id);
    List<String> roleIds = roleService.getRoleIdByUserId(id);
    roleIds.forEach(roleId -> System.out.println(roleId));
    userResponseDTO.setRoles(getRolesByIdIn(roleIds));
    return userResponseDTO;
  }

  @Override
  public UserPageResponseDTO listUsers(int size, int page, String keyword, boolean isAll) {
    log.info("(list)keyword: {}, size : {}, page: {}, isAll: {}", keyword, size, page, isAll);
    UserPageResponseDTO userPageResponseDTO = userService.list(size, page, keyword, isAll);
    userPageResponseDTO.getUserResponse().forEach(userResponseDTO ->
          userResponseDTO.setRoles(getRolesByIdIn(roleService.getRoleIdByUserId(userResponseDTO.getId()))));
    return userPageResponseDTO;
  }

  @Override
  public ShortcutUser getShortcutUser(String id) {
    ShortcutUser shortcutUser = userService.getShortcutUser(id);
    List<String> roleIds = roleService.getRoleIdByUserId(id);
    shortcutUser.setRoles(getShortcutRolesByIdIn(roleIds));
    return shortcutUser;
  }

  public ShortcutRole getShortcutRole(String roleId) {
    ShortcutRole shortcutRole = roleService.getShortcutRole(roleId);
    shortcutRole.setElements(elementService.listShortcutByRoleId(roleId));
    for (ShortcutElement element : shortcutRole.getElements()) {
      element.setPermissions(permissionService.getByElementIdAndRoleId(element.getId(), roleId));
    }
    return shortcutRole;
  }

  public List<ShortcutRole> getShortcutRolesByIdIn(List<String> roleIds) {
    List<ShortcutRole> shortcutRoles = new ArrayList<>();
    roleIds.forEach(roleId -> shortcutRoles.add(getShortcutRole(roleId)));
    return shortcutRoles;
  }

  @Override
  public ElementResponseDTO createElement(ElementRequestDTO request) {
    log.info("(create) request: {}", request);
    return elementService.create(request, permissionService.findByIdIn(request.getPermissionIds()));
  }

  private List<RoleElementPermission> buildRoleObjectPermissions(
        Role role, List<ElementIdRequestDTO> elementIdRequestDTOS
  ) {
    log.info("(buildRoleObjectPermissions) role: {}, elementIdRequestDTOS: {}", role, elementIdRequestDTOS);

    List<RoleElementPermission> roleObjectPermissions = new ArrayList<>();

    for (ElementIdRequestDTO element : elementIdRequestDTOS) {
      Element existElement = elementService.get(element.getId());
      List<Permission> permissions = permissionService.findByIdIn(element.getPermissionIds());
      for (Permission permission : permissions) {
        roleObjectPermissions.add(RoleElementPermission.of(role, existElement, permission));
      }
    }

    return roleObjectPermissions;
  }

  private void setElementAndPermissionForRole(RoleResponseDTO roleResponseDTO) {
    log.info("(setElementAndPermissionForRole) roleResponseDTO : {}", roleResponseDTO);
    List<ElementResponseDTO> elementResponseDTOS = elementService.getByRoleId(roleResponseDTO.getId());
    elementResponseDTOS.forEach(elementResponseDTO -> elementResponseDTO.setPermissions(
          permissionService.getByElementAndRoleId(elementResponseDTO.getId(), roleResponseDTO.getId())
    ));
    roleResponseDTO.setElements(elementResponseDTOS);
  }
}


