package com.ncsgroup.core.authentication.exception;


import com.ncs.core.exception.exception.base.ConflictException;

public class RoleAlreadyExistException extends ConflictException {

  public RoleAlreadyExistException() {
    setCode("com.ncsgroup.core.authentocation.exception.RoleAlreadyExistException");
  }

  public RoleAlreadyExistException(String object) {
    setCode("com.ncsgroup.core.authentocation.exception.RoleAlreadyExistException");
    addParam("id", object);
  }
}
