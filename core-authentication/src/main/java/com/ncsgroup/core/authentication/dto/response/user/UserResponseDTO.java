package com.ncsgroup.core.authentication.dto.response.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.ActiveStatus.ACTIVE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.VI_LANGUAGE;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UserResponseDTO {
  private String id;
  private String username;
  private String fullName;
  private String email;
  private String phoneNumber;
  private byte[] avatarLink;
  private String language = VI_LANGUAGE;
  private String theme;
  private int notificationEnable;
  private int lastNotificationId;
  private int isActive = ACTIVE;
  private List<RoleResponseDTO> roles;

  public UserResponseDTO(String id, String username, String fullName, String email, String phoneNumber, int isActive) {
    this.id = id;
    this.username = username;
    this.fullName = fullName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.isActive = isActive;
  }
}
