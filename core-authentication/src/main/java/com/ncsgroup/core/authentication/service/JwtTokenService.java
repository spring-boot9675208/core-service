package com.ncsgroup.core.authentication.service;

import java.util.Map;

public interface JwtTokenService {
  String generateAccessToken(String userId, Map<String, Object> claims);

  String generateRefreshToken(String userId, String username);

  String getSubjectFromToken(String token);

  Long getExpirationTime(String token);
  String getUsernameFromToken(String token);
}
