package com.ncsgroup.core.authentication.service;

import com.ncsgroup.core.authentication.dto.request.authen.RefreshTokenRequestDTO;
import com.ncsgroup.core.authentication.dto.response.authen.LoginResponseDTO;
import com.ncsgroup.core.authentication.dto.response.authen.TokenResponseDTO;

public interface AuthenticationService {
  LoginResponseDTO login(String username, String password);

  TokenResponseDTO refreshToken(RefreshTokenRequestDTO request);

}
