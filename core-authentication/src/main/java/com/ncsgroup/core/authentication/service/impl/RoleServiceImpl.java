package com.ncsgroup.core.authentication.service.impl;

import com.ncsgroup.core.authentication.dto.request.role.RoleRequestDTO;
import com.ncsgroup.core.authentication.dto.response.element.ElementResponseDTO;
import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.RoleResponseDTO;
import com.ncsgroup.core.authentication.dto.response.role.ShortcutRole;
import com.ncsgroup.core.authentication.entity.Role;
import com.ncsgroup.core.authentication.exception.RoleAlreadyExistException;
import com.ncsgroup.core.authentication.exception.role.RoleNotFoundException;
import com.ncsgroup.core.authentication.repository.RoleRepository;
import com.ncsgroup.core.authentication.service.RoleService;
import com.ncsgroup.core.authentication.service.base.BaseServiceImpl;
import com.ncsgroup.core.authentication.utils.MapperUtils;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ncsgroup.core.authentication.utils.MapperUtils.toDTO;


@Slf4j
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {
  private final RoleRepository repository;

  public RoleServiceImpl(RoleRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public void checkExistRoleName(String name) {
    log.info("(checkExistRoleName) name: {}", name);
    if (repository.existsByName(name)) {
      log.error("(checkExistRoleName) ======> roleAlreadyExistException roleName: {}", name);
      throw new RoleAlreadyExistException(name);
    }
  }

  @Override
  public List<RoleResponseDTO> getAll(String keyword, int page, int size, boolean isAll) {
    log.info("(list)name: {}, page : {}, size: {}, isAll: {}", keyword, page, size, isAll);
    Pageable pageable = PageRequest.of(page, size);
    List<RoleResponseDTO> roleResponseDTOS = isAll ? repository.listAll() : repository.search(pageable, keyword);
    return roleResponseDTOS;
  }

  @Override
  public void removeInRoleElementPermission(String id) {
    log.info("(removeInRoleElementPermission) id : {}", id);
    repository.deleteByRoleId(id);
  }

  @Override
  public RoleResponseDTO compositeRoleDTO(Role role) {
    log.info("(compositeRoleDTO) role: {}", role);
    Map<ElementResponseDTO, List<PermissionResponseDTO>> objectMap = new HashMap<>();

    for (var roleObjectPermission : role.getRoleElementPermissions()) {
      ElementResponseDTO object = toDTO(roleObjectPermission.getElement(), ElementResponseDTO.class);
      PermissionResponseDTO permission = toDTO(roleObjectPermission.getPermission(), PermissionResponseDTO.class);
      objectMap.computeIfAbsent(object, value -> new ArrayList<>()).add(permission);
    }

    List<ElementResponseDTO> objectResponses = new ArrayList<>();

    objectMap.forEach((objectResponseDTO, value) -> {
      objectResponseDTO.setPermissions(value);
      objectResponses.add(objectResponseDTO);
    });

    RoleResponseDTO roleResponseDTO = toDTO(role, RoleResponseDTO.class);
    roleResponseDTO.setElements(objectResponses);

    return roleResponseDTO;
  }

  @Override
  @Transactional
  public void delete(String id) {
    log.info("(delete) id : {}", id);
    Role role = find(id);
    repository.delete(role);
    removeInRoleElementPermission(id);
  }

  @Override
  @Transactional
  public Role checkPreUpdate(String id, RoleRequestDTO request) {
    log.info("(checkPreUpdate) id :{}, request:{}", id, request);
    Role role = find(id);
    if (!request.getName().equals(role.getName())) checkExistRoleName(request.getName());
    removeInRoleElementPermission(id);
    return MapperUtils.toEntity(request, Role.class);
  }

  @Override
  public RoleResponseDTO detail(String id) {
    log.info("(detail) id: {}", id);
    return MapperUtils.toDTO(find(id), RoleResponseDTO.class);
  }

  @Override
  public List<String> getRoleIdByUserId(String userId) {
    log.info("(getRoleIdByUserId) userId:{}", userId);
    return repository.getRoleIdByUserId(userId);
  }

  @Override
  public ShortcutRole getShortcutRole(String id) {
    return repository.getShortcutRole(id);
  }

  private Role find(String id) {
    log.info("(find) id : {}", id);
    return repository.findById(id).orElseThrow(RoleNotFoundException::new);
  }
}
