package com.ncsgroup.core.authentication.controller;


import com.ncsgroup.core.authentication.dto.request.user.ChangePasswordRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserRequestDTO;
import com.ncsgroup.core.authentication.dto.request.user.UserUpdateRequestDTO;
import com.ncsgroup.core.authentication.dto.response.ResponseGeneral;
import com.ncsgroup.core.authentication.dto.response.user.UserPageResponseDTO;
import com.ncsgroup.core.authentication.dto.response.user.UserResponseDTO;
import com.ncsgroup.core.authentication.facade.AdministrationFacadeService;
import com.ncsgroup.core.authentication.service.MessageService;
import com.ncsgroup.core.authentication.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.DEFAULT_LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.CommonConstants.LANGUAGE;
import static com.ncsgroup.core.authentication.constanst.AuthenticationConstants.MessageCode.*;


@Slf4j
@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
  private final UserService userService;
  private final MessageService messageService;
  private final AdministrationFacadeService facadeService;

  @PostMapping
  public ResponseGeneral<UserResponseDTO> create(
        @RequestBody UserRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_USER_SUCCESS, language),
          facadeService.createUser(request)
    );
  }

  @PutMapping("/{id}")
  public ResponseGeneral<UserResponseDTO> update(
        @PathVariable("id") String id,
        @RequestBody UserUpdateRequestDTO request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_USER_SUCCESS, language),
          facadeService.updateUser(id, request)
    );
  }

  @GetMapping
  public ResponseGeneral<UserPageResponseDTO> list(
        @RequestParam(name = "keyword", required = false) String keyword,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "all", defaultValue = "false", required = false) boolean isAll,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(list)keyword: {}, size : {}, page: {}, isAll: {}", keyword, size, page, isAll);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          facadeService.listUsers(size, page, keyword, isAll)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete) id: {}", id);
    userService.remove(id);
    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language));
  }

  @GetMapping("{id}")
  public ResponseGeneral<UserResponseDTO> get(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(get) id: {}", id);
    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language), facadeService.getDetailUser(id));
  }

  @PostMapping("/change-password")
  public ResponseGeneral<Void> changePassword(
        @RequestBody @Valid ChangePasswordRequestDTO requestDTO,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(changePassword) requestDTO: {}", requestDTO);
    userService.changePassword(requestDTO);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(CHANGE_PASSWORD_SUCCESS, language),
          null
    );
  }
}
