package com.ncsgroup.core.authentication.dto.request.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.core.authentication.validator.ValidationConfirmPassword;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;


@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@ValidationConfirmPassword(originalField = "newPassword", confirmationField = "confirmPassword")
public class ChangePasswordRequestDTO {
  private String userId;
  @NotBlank
  @Size(min = 8)
  private String password;
  @NotBlank
  @Size(min = 8)
  private String newPassword;
  @NotBlank
  @Size(min = 8)
  private String confirmPassword;
}
