package com.ncsgroup.core.authentication.dto.response.role;

import com.ncsgroup.core.authentication.dto.response.element.ShortcutElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ShortcutRole {
  private String id;
  private String name;
  private List<ShortcutElement> elements;

  public ShortcutRole(String id, String name) {
    this.id = id;
    this.name = name;
  }
}
