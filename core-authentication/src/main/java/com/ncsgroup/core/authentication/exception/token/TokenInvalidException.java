package com.ncsgroup.core.authentication.exception.token;

import com.ncs.core.exception.exception.base.BadRequestException;

public class TokenInvalidException extends BadRequestException {
  public TokenInvalidException() {
    setCode("com.ncsgroup.core.authentication.exception.token.TokenInvalidException");
  }
}
