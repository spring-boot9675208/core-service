package com.ncsgroup.core.authentication.exception.permission;


import com.ncs.core.exception.exception.base.BadRequestException;

public class PermissionNameAlreadyExistException extends BadRequestException {

  public PermissionNameAlreadyExistException() {
    setCode("com.ncsgroup.core.authentication.exception.permission.PermissionNameAlreadyExistException");
  }

  public PermissionNameAlreadyExistException(String code) {
    setCode("com.ncsgroup.core.authentication.exception.permission.PermissionNameAlreadyExistException");
    addParam("code", code);
  }

}
