package com.ncsgroup.core.authentication.configuration;


import com.ncs.core.redis.config.EnableCoreRedis;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

@Configuration
@EnableCoreRedis
public class RedisConfiguration {
  @Value("${spring.data.redis.host}")
  private String redisHost;
  @Value("${spring.data.redis.port}")
  private Integer redisPort;

  @Bean
  public LettuceConnectionFactory redisConnectionFactory() {
    LettuceConnectionFactory lcf = new LettuceConnectionFactory();
    lcf.setHostName(redisHost);
    lcf.setPort(redisPort);
    lcf.afterPropertiesSet();
    return lcf;
  }
}
