package com.ncsgroup.core.authentication.exception.user;

import com.ncs.core.exception.exception.base.ConflictException;

public class UserAlreadyException extends ConflictException {

  public UserAlreadyException(){
    setCode("com.ncsgroup.core.authentication.exception.user.UserAlreadyException");
  }
}
