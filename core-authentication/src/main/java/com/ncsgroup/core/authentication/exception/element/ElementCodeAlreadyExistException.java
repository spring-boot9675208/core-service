package com.ncsgroup.core.authentication.exception.element;


import com.ncs.core.exception.exception.base.BadRequestException;

public class ElementCodeAlreadyExistException extends BadRequestException {

  public ElementCodeAlreadyExistException() {
    setCode("com.ncsgroup.core.authentication.exception.element.ElementCodeAlreadyExistException");
  }

  public ElementCodeAlreadyExistException(String code) {
    setCode("com.ncsgroup.core.authentication.exception.element.ElementCodeAlreadyExistException");
    addParam("code", code);
  }

}
