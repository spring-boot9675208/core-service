package com.ncsgroup.core.authentication.repository;

import com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO;
import com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission;
import com.ncsgroup.core.authentication.entity.Permission;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PermissionRepository extends BaseRepository<Permission> {

  @Query(value = "SELECT new com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO( " +
        " p.id, p.code,p.name, p.description, p.isActive, p.permissionIdentity ) " +
        "FROM Permission p ")
  List<PermissionResponseDTO> list();
  @Query("SELECT new com.ncsgroup.core.authentication.dto.response.permission.ShortcutPermission ( " +
        " p.id, p.code, p.name) FROM Permission p JOIN p.roleElementPermissions rep " +
        "JOIN rep.element e JOIN rep.role r WHERE e.id = :elementId AND r.id = :roleId")
  List<ShortcutPermission> listByRoleIdAndElementId(String elementId, String roleId);

  @Query("SELECT new com.ncsgroup.core.authentication.dto.response.permission.PermissionResponseDTO(" +
        "p.id, p.code, p.name, p.description, p.isActive, p.permissionIdentity) " +
        "FROM Element e " +
        "JOIN e.permissions p " +
        "WHERE e.id= :id")
  List<PermissionResponseDTO> listByElementId(String id);

  List<Permission> findByIdIn(List<String> ids);

  @Query("SELECT p FROM Permission p JOIN p.roleElementPermissions rep JOIN rep.element e " +
        "JOIN rep.role r WHERE e.id = :elementId AND r.id = :roleId")
  List<Permission> listByElementIdAndRoleId(String elementId, String roleId);

  @Modifying
  @Query(value = "DELETE FROM element_permission WHERE permission_id = ?1", nativeQuery = true)
  void deleteInElementPermission(String id);

  boolean existsByCode(String code);

  boolean existsByName(String name);

}
