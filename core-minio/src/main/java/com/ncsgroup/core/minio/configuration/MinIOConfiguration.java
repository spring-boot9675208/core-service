package com.ncsgroup.core.minio.configuration;

import com.ncs.core.exception.configuration.EnableCoreApiException;
import com.ncsgroup.core.minio.service.MinIOService;
import com.ncsgroup.core.minio.service.impl.MinIOServiceImpl;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.ncsgroup.core.minio"})
@EnableCoreApiException
public class MinIOConfiguration {
  @Value("${application.minio.bucket-name}")
  private String bucketName;

  @Value("${application.minio.url}")
  private String minioUrl;

  @Value("${application.minio.access-key}")
  private String accessKey;

  @Value("${application.minio.secret-key}")
  private String secretKey;

  @Bean
  public MinioClient minioClient() {
    return MinioClient
          .builder()
          .endpoint(minioUrl)
          .credentials(accessKey, secretKey)
          .build();
  }

  @Bean
  public MinIOService minIOService(MinioClient minioClient) {
    return new MinIOServiceImpl(bucketName, minioClient);
  }
}
