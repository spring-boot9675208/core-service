package com.ncsgroup.core.minio.controller;


import com.ncs.core.exception.model.ResponseGeneral;
import com.ncsgroup.core.minio.model.File;
import com.ncsgroup.core.minio.service.MinIOService;
import io.minio.errors.*;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static com.ncsgroup.core.minio.constants.MinIOConstants.ContentType.IMAGE;
import static com.ncsgroup.core.minio.constants.MinIOConstants.ContentType.OCTET_STREAM;

@RestController
@Slf4j
@RequestMapping("/api/v1/minio/files")
@RequiredArgsConstructor
public class MinIOController {
  private final MinIOService minIOService;

  @PostMapping("/upload")
  public ResponseGeneral<File> upload(@RequestParam(name = "file") MultipartFile file) {
    return ResponseGeneral.ofSuccess(null, minIOService.putFile(file));
  }


  @GetMapping
  public ResponseGeneral<byte[]> get(
        @RequestParam(name = "bucket") String bucketName,
        @RequestParam(name = "filename") String filename
  ) {
    log.info("(get) ");
    return ResponseGeneral.ofSuccess(null, minIOService.getFile(filename, bucketName));
  }

  @GetMapping("/pre-sign")
  public ResponseGeneral<String> preSign(
        @RequestParam(name = "filename") String filename
  ) {
    return ResponseGeneral.ofSuccess(null, minIOService.getPreSignedUrl(filename, IMAGE));
  }

  @GetMapping("/{bucket-name}/{filename}")
  public ResponseEntity<Void> download(
        @PathVariable(name = "bucket-name") String bucketName,
        @PathVariable(name = "filename") String filename,
        HttpServletResponse response
  ) throws IOException {
    response.setContentType(OCTET_STREAM);
    response.setHeader("Content-Disposition", "attachment;filename=".concat(filename));
    OutputStream out = response.getOutputStream();
    out.write(minIOService.getFile(filename));

    return ResponseEntity.ok().build();
  }
}
