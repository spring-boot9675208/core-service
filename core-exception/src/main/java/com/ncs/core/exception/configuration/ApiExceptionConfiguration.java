package com.ncs.core.exception.configuration;


import com.ncs.core.exception.advice.ApiAdviceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import static com.ncs.core.exception.exception.ExceptionConstants.ENCODING_UTF_8;

@Configuration
@ComponentScan(basePackages = {"com.ncs.core.exception"})
@Slf4j
public class ApiExceptionConfiguration {
  @Bean
  public MessageSource messageSourceException() {
    var messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:i18n/messages");
    messageSource.setDefaultEncoding(ENCODING_UTF_8);
    return messageSource;
  }
}
