package com.ncs.core.exception.exception.base;


import static com.ncs.core.exception.exception.ExceptionConstants.BAD_REQUEST;

public class BadRequestException extends BaseException {
  public BadRequestException() {
    setCode("com.ncsgroup.core.authentication.exception.base.BadRequestException");
    setStatus(BAD_REQUEST);
  }
}
